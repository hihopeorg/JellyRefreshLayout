/**
 * The MIT License (MIT)
 *
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.co.imallan.jellyrefreshlayout;

import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Assert;
import uk.co.imallan.jellyrefresh.JellyRefreshLayout;

public class JellyRefreshLayoutUITest  extends TestCase {

    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    public void setUp() throws Exception {
        super.setUp();
        mContext = sAbilityDelegator.getAppContext();
    }

    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    public void testClickRefresh() {

        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1);
        Text menu = (Text) mainAbility.findComponentById(ResourceTable.Id_menu);
        JellyRefreshLayout refreshLayout = (JellyRefreshLayout) mainAbility.findComponentById(ResourceTable.Id_jelly_refresh);
        sleep(1);
        EventHelper.triggerClickEvent(mainAbility, menu);
        sleep(5);
        Assert.assertFalse("It's OK.",refreshLayout.isRefreshing());

    }

    public void testMoveRefresh() {

        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1);
        JellyRefreshLayout refreshLayout = (JellyRefreshLayout) mainAbility.findComponentById(ResourceTable.Id_jelly_refresh);
        sleep(1);
        EventHelper.inputSwipe(mainAbility, 432,533,432,1490,100);
        sleep(5);
        Assert.assertFalse("It's OK.",refreshLayout.isRefreshing());

    }



    private void sleep(int duration) {
        try {
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
