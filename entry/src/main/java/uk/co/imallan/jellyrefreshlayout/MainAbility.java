package uk.co.imallan.jellyrefreshlayout;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import uk.co.imallan.jellyrefresh.JellyRefreshLayout;
import uk.co.imallan.jellyrefresh.PullToRefreshLayout;

public class MainAbility extends Ability implements Component.ClickedListener {

    private JellyRefreshLayout mJellyLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#ff009688"));
        findComponentById(ResourceTable.Id_menu).setClickedListener(this);
        mJellyLayout = (JellyRefreshLayout) findComponentById(ResourceTable.Id_jelly_refresh);
        mJellyLayout.setPullToRefreshListener(new PullToRefreshLayout.PullToRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {

                new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                    @Override
                    public void run() {
                        mJellyLayout.setRefreshing(false);
                    }
                }, 3000);
            }
        });
        Component loadingView = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_view_loading, null, false);
        mJellyLayout.setLoadingView(loadingView);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                mJellyLayout.setRefreshing(true);
            }
        });
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                mJellyLayout.setRefreshing(false);
            }
        }, 3000);
    }
}
