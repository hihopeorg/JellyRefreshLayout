package uk.co.imallan.jellyrefresh;

import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;
import uk.co.imallan.jellyrefresh.annotation.ColorInt;

public class JellyLayout extends StackLayout implements Component.DrawTask {

    private Paint mPaint;
    private Path mPath;
    @ColorInt
    private int mColor = Color.GRAY.getValue();
    private Color newColor = new Color(mColor);
    private float mPointX;
    float mHeaderHeight = 0;
    float mPullHeight = 0;

    public JellyLayout(Context context) {
        this(context, null);
    }

    public JellyLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
        addDrawTask(this);
    }


    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);

        mPath = new Path();
    }

    public void setColor(int color) {
        newColor = new Color(color);
    }

    public void setPointX(float pointX) {
        boolean needInvalidate = pointX != mPointX;
        mPointX = pointX;
        if (needInvalidate) invalidate();
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {

        drawPulling(canvas);
    }

    private void drawPulling(Canvas canvas) {

        final int width = getWidth();
        final float mDisplayX = (mPointX - width / 2f) * 0.5f + width / 2f;

        mPaint.setColor(newColor);

        int headerHeight = (int) mHeaderHeight;
        int pullHeight = (int) mPullHeight;
        mPath.rewind();
        mPath.moveTo(0, 0);
        mPath.lineTo(0, headerHeight);
        mPath.quadTo(mDisplayX, pullHeight, width, headerHeight);
        mPath.lineTo(width, 0);
        mPath.close();

        canvas.drawPath(mPath, mPaint);
    }

    public void setHeaderHeight(float headerHeight) {
        mHeaderHeight = headerHeight;
    }

}
