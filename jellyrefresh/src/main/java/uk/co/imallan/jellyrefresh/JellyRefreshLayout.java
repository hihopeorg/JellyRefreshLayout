package uk.co.imallan.jellyrefresh;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import uk.co.imallan.jellyrefresh.annotation.Nullable;

public class JellyRefreshLayout extends PullToRefreshLayout implements PullToRefreshLayout.PullToRefreshPullingListener {

    private JellyLayout mJellyLayout;
    private Component mLoadingView;

    public JellyRefreshLayout(Context context) {
        this(context, null);
    }

    public JellyRefreshLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
        resolveAttributes(attrs);
    }


    private void resolveAttributes(@Nullable AttrSet attrs) {
        if (attrs == null) return;

        if (attrs.getAttr("jellyColor").isPresent()) {
            int color = attrs.getAttr("jellyColor").get().getColorValue().getValue();
            mJellyLayout.setColor(color);
        }
        if (attrs.getAttr("headerHeight").isPresent()) {
            int height = attrs.getAttr("headerHeight").get().getDimensionValue();
            mHeaderHeight = height;
        }
        if (attrs.getAttr("pullHeight").isPresent()) {
            float pullHeight = attrs.getAttr("pullHeight").get().getDimensionValue();
            mPullHeight = pullHeight;
        }
        if (attrs.getAttr("triggerHeight").isPresent()) {
            float triggerHeight = attrs.getAttr("triggerHeight").get().getDimensionValue();
            mTriggerHeight = triggerHeight;
        }
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        if (refreshing) {
            mJellyLayout.setPointX(mJellyLayout.getWidth() / 2);
        }
        super.setRefreshing(refreshing);
    }

    private void init() {
        mJellyLayout = new JellyLayout(getContext());
        StackLayout.LayoutConfig params = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
        mJellyLayout.setLayoutConfig(params);

        setHeaderView(mJellyLayout);
        setPullingListener(this);
    }

    @Override
    protected void onStateChanged(@State int newState) {
        switch (newState) {
            case STATE_REFRESHING:
                if (mLoadingView != null) {
                    mLoadingView.setVisibility(VISIBLE);
                }
                break;
            case STATE_REFRESHING_SETTLING:
                if (mLoadingView != null) {
                    mLoadingView.setContentPositionY(mHeaderHeight);
                    mLoadingView.setVisibility(VISIBLE);
                    mLoadingView.setTranslationY(mPullHeight);
                    mLoadingView.createAnimatorProperty().moveFromY(mHeaderHeight).moveToY(0).setDuration(150).start();
                }
                break;
            case STATE_DRAGGING:
            case STATE_IDLE:
            case STATE_SETTLING:
            case STATE_RELEASING:
                if (mLoadingView != null) {
                    mLoadingView.setVisibility(INVISIBLE);
                }
                break;
        }
    }


    @Override
    public void onPulling(float fraction, float pointXPosition) {
        mJellyLayout.setPointX(pointXPosition);
    }

    @Override
    public void onTranslationYChanged(float translationY) {
        switch (getState()) {
            case STATE_DRAGGING:
            case STATE_RELEASING:
                mJellyLayout.mHeaderHeight = Math.min(translationY / 2, mHeaderHeight);
                mJellyLayout.mPullHeight = translationY;
                break;
            case STATE_REFRESHING:
                mJellyLayout.mHeaderHeight = mHeaderHeight;
                mJellyLayout.mPullHeight = mHeaderHeight;
                break;
            case STATE_SETTLING:
                mJellyLayout.mHeaderHeight = translationY;
                mJellyLayout.mPullHeight = translationY;
                break;
            case STATE_REFRESHING_SETTLING:
                mJellyLayout.mHeaderHeight = mHeaderHeight;
                if (translationY > mHeaderHeight) {
                    float dy = translationY - mHeaderHeight;
                    float acceleratedHeight = translationY - 2 * dy;
                    mJellyLayout.mPullHeight = Math.max(mHeaderHeight, acceleratedHeight);
                } else {
                    mJellyLayout.mPullHeight = mHeaderHeight;
                }
                break;
            case STATE_IDLE:
                mJellyLayout.mHeaderHeight = 0;
                mJellyLayout.mPullHeight = 0;
                break;
        }
        mJellyLayout.invalidate();
    }

    public void setLoadingView(Component view) {
        StackLayout.LayoutConfig params = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        params.alignment = LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.TOP;
        mHeader.addComponent(view, params);
        view.setVisibility(INVISIBLE);
        mLoadingView = view;
    }
}
