package uk.co.imallan.jellyrefresh;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import ohos.system.version.SystemVersion;
import uk.co.imallan.jellyrefresh.annotation.IntDef;
import uk.co.imallan.jellyrefresh.annotation.NonNull;

import static ohos.agp.animation.Animator.CurveType.DECELERATE;

/**
 * Created by yilun
 * on 09/07/15.
 */
public class PullToRefreshLayout extends StackLayout {


    static final int STATE_IDLE = 0;
    static final int STATE_DRAGGING = 1;
    static final int STATE_RELEASING = 2;
    static final int STATE_SETTLING = 3;
    static final int STATE_REFRESHING = 4;
    static final int STATE_REFRESHING_SETTLING = 5;
    static final long REL_DRAG_DUR = 200;


    @IntDef({STATE_IDLE,
            STATE_DRAGGING,
            STATE_REFRESHING,
            STATE_RELEASING,
            STATE_REFRESHING_SETTLING,
            STATE_SETTLING})
    @interface State {
    }

    private float mTouchStartY;
    private float mCurrentY;
    private Component mChildView;
    private Context mContext;
    float mPullHeight;
    float mHeaderHeight;
    float mTriggerHeight;
    @State
    private int mState = STATE_IDLE;
    private PullToRefreshListener mPullToRefreshListener;
    private PullToRefreshPullingListener mPullToRefreshPullingListener;
    protected StackLayout mHeader;
    protected AnimatorValue mUpBackAnimator;
    private AnimatorGroup animatorGroup = null;
    private AnimatorProperty animatorProperty = null;


    public PullToRefreshLayout(Context context) {
        this(context, null);
    }

    public PullToRefreshLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        this.mContext = context;
        if (SystemVersion.getApiVersion() >= 7) {
            this.setForwardTouchListener(new ForwardTouchListener() {
                @Override
                public boolean onForwardTouch(Component component, TouchEvent touchEvent) {
                    return onTouch(component, touchEvent);
                }
            });
        } else {
            this.setTouchEventListener(new TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                    return onTouch(component, touchEvent);
                }
            });
        }
        init();
    }


    private void init() {
        if (getChildCount() >= 1) {
            throw new RuntimeException("You can only attach one child");
        }
        mPullHeight = AttrHelper.vp2px(150, mContext);
        mHeaderHeight = AttrHelper.vp2px(56, mContext);

        mTriggerHeight = mHeaderHeight;
        addHeaderContainer();

    }

    public void setHeaderView(Component headerView) {
        mHeader.addComponent(headerView);
    }

    private void addHeaderContainer() {
        StackLayout headerContainer = new StackLayout(getContext());
        LayoutConfig layoutParams = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        headerContainer.setLayoutConfig(layoutParams);
        mHeader = headerContainer;
        addViewInternal(headerContainer);
        setUpChildViewAnimator();
    }

    private void setUpChildViewAnimator() {
        if (mChildView == null) {
            return;
        }
        animatorProperty = mChildView.createAnimatorProperty();
        mUpBackAnimator = new AnimatorValue();
        mUpBackAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (mPullToRefreshPullingListener != null) {
                    mPullToRefreshPullingListener.onTranslationYChanged(mPullHeight - v * mPullHeight);
                }
            }
        });

    }

    private void addViewInternal(@NonNull Component child) {
        super.addComponent(child);
    }

    @Override
    public void addComponent(Component childComponent) {
        if (getChildCount() > 1) {
            throw new RuntimeException("You can only attach one child");
        }
        mChildView = childComponent;
        super.addComponent(childComponent);
        setUpChildViewAnimator();
    }

    public boolean canChildScrollUp() {
        if (mChildView == null) {
            return false;
        }
        return mChildView.canScroll(AXIS_Y);
    }

    @State
    public int getState() {
        return mState;
    }

    public void setState(@State int state) {
        if (mState != state) {
            mState = state;
            onStateChanged(mState);
        }
    }

    protected void onStateChanged(@State int newState) {
    }

    public boolean isRefreshing() {
        return getState() == STATE_REFRESHING;
    }

    float offsetY;

    public boolean onTouch(Component component, TouchEvent e) {
        if (getState() == STATE_REFRESHING
                || getState() == STATE_RELEASING
                || getState() == STATE_SETTLING
                || getState() == STATE_REFRESHING_SETTLING) {
            return true;
        }

        switch (e.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mTouchStartY = e.getPointerPosition(0).getY();
                mCurrentY = mTouchStartY;
                return true;
            case TouchEvent.POINT_MOVE:
                float currentY = e.getPointerPosition(0).getY();
                float dyy = currentY - mTouchStartY;
                if (dyy > 0 && mChildView.getScrollValue(AXIS_Y) > 0) {
                    return true;
                }
                setState(STATE_DRAGGING);
                mCurrentY = e.getPointerPosition(0).getY();
                float currentX = e.getPointerPosition(0).getX();
                float dy = MathUtils.constrains(
                        0,
                        mPullHeight * 2,
                        mCurrentY - mTouchStartY);
                if (mChildView != null) {
                    offsetY = dy / mPullHeight / 2 * dy / 2;
                    mChildView.setTranslationY(offsetY);
                    if(dyy < 0){
                        return true;
                    }
                    if (mPullToRefreshPullingListener != null) {
                        mPullToRefreshPullingListener.onTranslationYChanged(offsetY);
                        mPullToRefreshPullingListener.onPulling(offsetY / mHeaderHeight, currentX);
                    }
                }
                return false;
            case TouchEvent.CANCEL:
            case TouchEvent.PRIMARY_POINT_UP:
                if (mChildView != null) {
                    if (mChildView.getTranslationY() >= mTriggerHeight) {
                        animatorProperty.setCurveType(DECELERATE)
                                .moveFromY(offsetY)
                                .moveToY(mHeaderHeight)
                                .setStateChangedListener(new Animator.StateChangedListener() {
                                    @Override
                                    public void onStart(Animator animator) {
                                        setState(STATE_REFRESHING_SETTLING);
                                        mChildView.setEnabled(false);
                                    }

                                    @Override
                                    public void onStop(Animator animator) {
                                    }

                                    @Override
                                    public void onCancel(Animator animator) {
                                        setState(STATE_REFRESHING);
                                    }

                                    @Override
                                    public void onEnd(Animator animator) {
                                        setState(STATE_REFRESHING);
                                    }

                                    @Override
                                    public void onPause(Animator animator) {
                                    }

                                    @Override
                                    public void onResume(Animator animator) {
                                    }
                                });

                        startAnimatorGroup(animatorProperty, mUpBackAnimator);

                        if (mPullToRefreshListener != null) {
                            mPullToRefreshListener.onRefresh(this);
                        }
                    } else {
                        animatorProperty.moveFromY(offsetY).moveToY(0).setStateChangedListener(new Animator.StateChangedListener() {
                            @Override
                            public void onStart(Animator animator) {
                                setState(STATE_RELEASING);
                                setState(STATE_IDLE);
                            }

                            @Override
                            public void onStop(Animator animator) {
                            }

                            @Override
                            public void onCancel(Animator animator) {
                                setState(STATE_IDLE);
                            }

                            @Override
                            public void onEnd(Animator animator) {
                                setState(STATE_IDLE);
                            }

                            @Override
                            public void onPause(Animator animator) {
                            }

                            @Override
                            public void onResume(Animator animator) {

                            }
                        });

                        startAnimatorGroup(animatorProperty, mUpBackAnimator);
                    }

                } else {
                    setState(STATE_IDLE);
                }
                return true;
            default:
                return true;
        }
    }


    public void setPullToRefreshListener(PullToRefreshListener pullToRefreshListener) {
        this.mPullToRefreshListener = pullToRefreshListener;
    }

    public void setPullingListener(PullToRefreshPullingListener pullingListener) {
        this.mPullToRefreshPullingListener = pullingListener;
    }

    public void setRefreshing(boolean refreshing) {
        if (refreshing) {
            if (mChildView != null) {
                animatorProperty.moveFromY(0).moveToY(mHeaderHeight).setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                        setState(STATE_SETTLING);
                        mChildView.setEnabled(false);
                    }

                    @Override
                    public void onStop(Animator animator) {
                    }

                    @Override
                    public void onCancel(Animator animator) {
                        setState(STATE_REFRESHING);
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        setState(STATE_REFRESHING);
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });

                mUpBackAnimator.setValueUpdateListener(null);
                mUpBackAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        if (mPullToRefreshPullingListener != null) {
                            mPullToRefreshPullingListener.onTranslationYChanged(v * mHeaderHeight);
                        }
                    }
                });

                startAnimatorGroup(animatorProperty, mUpBackAnimator);

            }
        } else {
            if (!isRefreshing()) return;
            if (mChildView != null) {
                animatorProperty.moveFromY(mHeaderHeight).moveToY(0).setStateChangedListener(null).setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                        setState(STATE_SETTLING);
                    }

                    @Override
                    public void onStop(Animator animator) {
                    }

                    @Override
                    public void onCancel(Animator animator) {
                        setState(STATE_IDLE);
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        setState(STATE_IDLE);
                        mChildView.setEnabled(true);
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });

                mUpBackAnimator.setValueUpdateListener(null);
                mUpBackAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        if (mPullToRefreshPullingListener != null) {
                            mPullToRefreshPullingListener.onTranslationYChanged(mHeaderHeight - v * mHeaderHeight);
                        }
                    }
                });

                startAnimatorGroup(animatorProperty, mUpBackAnimator);

            } else {
                setState(STATE_IDLE);
            }
        }
    }

    private void startAnimatorGroup(AnimatorProperty animatorProperty, AnimatorValue animatorValue) {

        if (animatorGroup == null) {
            animatorGroup = new AnimatorGroup();
            animatorGroup.setCurveType(DECELERATE);
        }

        animatorGroup.clear();
        animatorGroup.cancel();

        animatorGroup.runParallel(animatorProperty, animatorValue);
        animatorGroup.start();
    }


    public interface PullToRefreshListener {

        void onRefresh(PullToRefreshLayout pullToRefreshLayout);

    }

    interface PullToRefreshPullingListener {

        void onPulling(float fraction, float pointXPosition);

        void onTranslationYChanged(float translationY);
    }
}
