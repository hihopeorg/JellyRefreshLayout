# JellyRefreshLayout

**本项目是基于开源项目 JellyRefreshLayout 进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/imallan/JellyRefreshLayout ）追踪到原项目版本**

#### 项目介绍

- 项目名称：JellyRefreshLayout
- 所属系列：ohos的第三方组件适配移植
- 功能：一个下拉刷新布局效果。
- 项目移植状态：完成
- 调用差异：无差异。
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/imallan/JellyRefreshLayout
- 基线release版本：V2.0,SHA1:ec0a02d70dea3a3e032aa0f6c0fbe20b930dca48
- 编程语言：Java
- 外部库依赖：无

#### 效果展示
![gif](preview.gif)

#### 安装教程

##### 方案一：

1. 编译har包JellyRefreshLayout_ohos.har。

2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'uk.co.imallan.ohos:JellyRefreshLayout:1.0.0'
 }
```


#### 使用说明：
有关该项目的业务实现，请参见“entry”文件夹.

##### 第一步: 编写布局文件
```java
    <uk.co.imallan.jellyrefresh.JellyRefreshLayout
        ohos:id="$+id:jelly_refresh"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:text="Loading…"
        app:jellyColor="#ff80cbc4"
        app:pullHeight="160vp"
        app:triggerHeight="108vp">

        <ScrollView
            ohos:height="match_parent"
            ohos:width="match_parent">

           ....  // 此处的父布局也可以是 Listcontainer

        </ScrollView>

    </uk.co.imallan.jellyrefresh.JellyRefreshLayout>
```
##### 第二步: 设置触发刷新时回调
```java
mJellyLayout.setRefreshListener(new JellyRefreshLayout.JellyRefreshListener() {
    @Override
    public void onRefresh(final JellyRefreshLayout jellyRefreshLayout) {
        // your code here
    }
});
```
当你完成时，完成刷新
```java
mJellyLayout.setRefreshing(false); // true 开始刷新
```
#### 版本迭代

- v1.0.0

1.目前支持功能如下：

```
- 手动下拉刷新
- 触发刷新api即可刷新，例如点击一个按钮调用刷新效果
```
#### 许可信息

    MIT License (MIT)
